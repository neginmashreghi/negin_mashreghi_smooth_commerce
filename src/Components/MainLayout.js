import React, { Component } from 'react'
import { Route, withRouter, Link } from "react-router-dom";
import { Layout, Menu } from 'antd';
import {
  DesktopOutlined,
  PieChartOutlined,
  FileOutlined,
  TeamOutlined,
  UserOutlined,
} from '@ant-design/icons';

import "./mainLayout.css"

import Overview from "./Overview"


const { Header, Content, Footer, Sider } = Layout;
const { SubMenu } = Menu;
class MainLayout extends Component {

    state = {
        collapsed: false,
      };
    
      onCollapse = collapsed => {
        console.log(collapsed);
        this.setState({ collapsed });
      };

    render() {
        const { collapsed } = this.state;
        return (
             <Layout style={{ minHeight: '100vh' }}>
        <Sider collapsible collapsed={collapsed} onCollapse={this.onCollapse}>
          <div className="logo" style={{ height: "64px",backgroundColor:"black", padding:"1%"}}  />
          <Menu theme="dark" defaultSelectedKeys={['1']} mode="inline">
            <Menu.Item key="1" icon={<PieChartOutlined />}>
              Overview
              <Link to={`/overview`}/>
            </Menu.Item>
           
           
          </Menu>
        </Sider>
        <Layout className="site-layout">
          <Header className="site-layout-background" style={{ padding: 0 }} />
          
          <Content style={{ margin: '24px 16px 0' }}>
          <Route path={`/overview`} component={Overview} />
        
       
            
          </Content>
          <Footer style={{ textAlign: 'center' }}>Ant Design ©2018 Created by Ant UED</Footer>
        </Layout>
      </Layout>
        )
    }
}


export default withRouter(MainLayout);