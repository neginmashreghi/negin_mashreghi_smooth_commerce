
## Challeneg
Front End Developer Challenge
Your challenge is to build a single-page application using React and Redux. The singlepage application will consume data from an API and display it to a user. Use any library
you want for fetching data from API. Although Redux is overkill for this project, we want
to gain a sense of your understanding of the library.
Feel free to use any boilerplate repo to get the project up and running fast.
Your application will have 2 pages. The first page will display an overview of aggregated data and the second will display the specific details of an entry selected from the
first page. See the attached mock-up and API endpoint for specific details.
Basic styling can be done anyway. Inline styles or via className and external .css file.
Coding Evaluation Criteria:
● Functional code that consumes the provided API
● Appropriate use of Redux
● Appropriately named functions / components
Bonus:
● Add routing via react-router library
● Style using styled-components library
● Using CSS Grid
## Install Requirements `npm install`
## Run Application `npm start`


