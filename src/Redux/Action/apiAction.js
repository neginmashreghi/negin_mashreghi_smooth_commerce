import { 
    GET_ALL_BUSINESSES,
    GET_ALL_BUSINESSES_ERROR
  } from "../types";

import axios from "axios";

export const getAllBusinesses = () => dispatch => {
    axios
      .get(`https://api.mocki.io/v1/276a0d5c`)
      .then(res => {
        dispatch({
          type: GET_ALL_BUSINESSES,
          payload: res.data
        });
      })
    .catch(err =>
        dispatch({
          type: GET_ALL_BUSINESSES_ERROR,
          payload: err
        })
      ); 
  };