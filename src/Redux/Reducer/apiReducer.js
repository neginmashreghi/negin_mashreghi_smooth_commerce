import { 
    GET_ALL_BUSINESSES,
    GET_ALL_BUSINESSES_ERROR
  } from "../types";

  const initialState = {
   allBusinesses:[],
   error:[]
  };

  export default function (state = initialState, action) {
    switch (action.type) {
      
      case GET_ALL_BUSINESSES_ERROR:
        return {
          ...state,
          respond: action.payload
        };
        case GET_ALL_BUSINESSES:
        return {
          ...state,
          markers: action.payload
        };
       
     
      default:
        return state;
    }
  }
