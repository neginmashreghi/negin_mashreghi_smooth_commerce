import React from 'react';
import MainLayout from "./Components/MainLayout"
import { Provider } from "react-redux";
import { BrowserRouter as Router, Route, Switch } from "react-router-dom";
import './App.css';
import store from "./Redux/store";

function App() {
  return (
    <Provider store={store}>
    <div className="App">
       <Router>
      <Switch>
        <Route path="/" component={MainLayout} />
      </Switch>
    </Router>
 
    
    </div>
    </Provider>
  );
}

export default App;
