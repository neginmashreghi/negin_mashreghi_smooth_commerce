import React, { Component } from 'react'
import { Table,Button, Modal} from 'antd';
import { connect } from "react-redux";
import {getAllBusinesses } from "../Redux/Action/apiAction"

import {
 
    FullscreenOutlined,

  } from '@ant-design/icons';

const mapStateToProps = state => {
    const mystate = state.handelApi
    return { 
        allBusinesses: mystate.markers ,
        error: mystate.error
    };
  };
  
const mapDispatchToProps = (dispatch) => {
    return {
        getAllBusinesses: () => dispatch(getAllBusinesses()),
        
    }
}
 class Overview extends Component {
    constructor(props) {
        super(props);
        this.state = {
            data:[],
            visible:false,
            store_hours: [],
            name: '',
            address:'',
            logo_url: ''
        };
      }

    componentDidMount(){
        // call action
        this.props.getAllBusinesses()
        console.log(this.props.allBusinesses)
        this.setState({ data : this.props.allBusinesses}); 

    }
    
    componentDidUpdate(prevProps, prevStat){
        if (prevProps.allBusinesses !== this.props.allBusinesses) {
            console.log(this.props.allBusinesses)
            this.setState({ data : this.props.allBusinesses});      
        }  
    }

    handelOverview =(record)=>{

        this.setState(
        {
            
            visible:true,
            store_hours: record.store_hours,
            name: record.name,
            address: record.address,
            logo_url: record.logo_url
        })

    }
    onCancel=()=>
    {
        this.setState(
            {visible:false})

    }


    render() {
        const {data , visible , logo_url, name, address, store_hours} = this.state
        const columns = [
          {
            title: 'Business Name',
            dataIndex: 'name',
            key: 'name',
            
          },
          {
            title: 'Address',
            dataIndex: 'address',
            key: 'address',
            
          },
          {
            title: 'Website',
            dataIndex: 'website',
            key: 'website',
            
          },
          {
            title: 'Operation',
            dataIndex: 'operation',
            render: (text, record) =>
              this.state.data.length >= 1 ? (
                  <div>   
                       <Button style={{marginRight:"10px"}} type="primary" shape="circle" icon={<FullscreenOutlined />} onClick={() => this.handelOverview(record)} />         
                       
                </div>   
              ) : null,},
           ];

   
        return (
            <div  style={{width: "100%", backgroundColor: "white"}}>
                <Table 
                style={ { margin:"2%"}}
                columns={columns} 
                dataSource={data} 
                bordered
                pagination={false}
                />
                 <Modal
                    title="Overview"
                    centered={true}

                    visible={this.state.visible}
                    footer={null}
                        onCancel={this.onCancel}
                    >
                    <img src={logo_url} alt="Italian Trulli" style={{width: "200px", height:"200px"}} />
                    <p><b>Name :</b></p>
                    <p>{name}</p>
                    <p><b>Addres:</b></p>
                    <p>{address}</p>
                    <p><b>Hours:</b></p>
                    {
                        store_hours.map((time, index)=>{
                            return(
                                <p key={index}> {time.day}: {time.end_time} am - {time.start_time} pm</p>
                            )
                        })
                    }
                </Modal>
            </div>
        )
    }
}

const OverviewWrapper = connect(mapStateToProps, mapDispatchToProps)(Overview);
export default OverviewWrapper